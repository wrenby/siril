{
    "app-id": "org.free_astro.siril",
    "runtime": "org.gnome.Platform",
    "runtime-version": "master",
    "sdk": "org.gnome.Sdk",
    "command": "siril",
    "rename-icon": "org.free_astro.siril",
    "finish-args": [
        "--share=ipc",
        "--share=network",
        "--socket=fallback-x11",
        "--socket=wayland",
        "--filesystem=home",
        /* Needed for gvfs to work */
        "--talk-name=org.gtk.vfs.*",
        "--filesystem=xdg-run/gvfs",
        "--filesystem=xdg-run/gvfsd"
    ],
    "modules": [
        {
            "name": "cfitsio",
            "config-opts": [
                "--enable-reentrant"
            ],
            "make-args": ["shared"],
            "cleanup": [
                "/include",
                "/lib/*.a",
                "/lib/*.la",
                "/lib/pkgconfig"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "http://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio-4.1.0.tar.gz",
                    "sha256": "b367c695d2831958e7166921c3b356d5dfa51b1ecee505b97416ba39d1b6c17a"
                }
            ]
        },
        {
            "name": "wcslib",
            "config-opts": [
                "LIBS=-pthread -lcurl -lm",
                "--without-pgplot",
                "--without-cfitsio",
                "--disable-fortran"
            ],
            "cleanup": [
                "/include",
                "/lib/*.a",
                "/lib/*.la",
                "/lib/pkgconfig"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "ftp://ftp.atnf.csiro.au/pub/software/wcslib/wcslib-7.11.tar.bz2",
                    "sha256": "46befbfdf50cd4953896676a7d570094dc7661e2ae9677b092e7fb13cee3da5f"
                }
            ]
        },
        {
            "name": "gsl",
            "config-opts": [
                "--enable-shared",
                "--disable-static"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://mirrors.ibiblio.org/gnu/gsl/gsl-2.7.1.tar.gz",
                    "sha256": "dcb0fbd43048832b757ff9942691a8dd70026d5da0ff85601e52687f6deeb34b"
                }
            ],
            "cleanup": [
                "/bin",
                "/include",
                "/lib/pkgconfig",
                "/lib/*.a",
                "/lib/*.la",
                "/share/aclocal",
                "/share/info",
                "/share/man"
            ]
        },
        {
            "name": "fftw3",
            "buildsystem": "autotools",
             "build-options": {
                "arch": {
                    "x86_64": {
                        "config-opts": [
                            "--enable-sse2",
                            "--enable-avx",
                            "--enable-avx-128-fma"
                        ]
                    },
                    "aarch64": {
                        "config-opts": [
                            "--enable-neon"
                        ]
                    }
                }
            },           
            "config-opts": [
                "--enable-float",
                "--enable-threads",
                "--enable-openmp",
                "--disable-fortran"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "http://fftw.org/fftw-3.3.10.tar.gz",
                    "sha256": "56c932549852cddcfafdab3820b0200c7742675be92179e59e6215b340e26467"
                }
            ],
            "cleanup": [
                "/bin",
                "/include",
                "/lib/*.a",
                "/lib/*.la",
                "/lib/cmake",
                "/lib/pkgconfig",
                "/share/info",
                "/share/man"
            ]
        },
        {
            "name": "exiv2",
            "buildsystem": "cmake-ninja",
            "config-opts": ["-DCMAKE_BUILD_TYPE=Release"],
            "cleanup": [
                "/bin",
                "/include",
                "/lib/cmake",
                "/lib/pkgconfig",
                "/lib/*.a",
                "/share/man"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://github.com/Exiv2/exiv2/releases/download/v0.27.5/exiv2-0.27.5-Source.tar.gz",
                    "sha256": "35a58618ab236a901ca4928b0ad8b31007ebdc0386d904409d825024e45ea6e2"
                }
            ]
        },
        {
            "name": "libraw",
            "config-opts": [ "--disable-examples",
                             "--disable-jasper",
                             "--disable-static",
                             "--enable-jpeg",
                             "--enable-lcms",
                             "--enable-openmp" ],
            "cleanup": [ "/share/doc" ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://www.libraw.org/data/LibRaw-0.20.2.tar.gz",
                    "sha256": "dc1b486c2003435733043e4e05273477326e51c3ea554c6864a4eafaff1004a6"
                },
                {
                    "type": "shell",
                    "commands": [ "autoreconf -vfi" ]
                }
            ]
        },
        {
            "name": "libconfig",
            "config-opts": [
                "--disable-static",
                "--enable-shared"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://hyperrealm.github.io/libconfig/dist/libconfig-1.7.3.tar.gz",
                    "sha256": "545166d6cac037744381d1e9cc5a5405094e7bfad16a411699bcff40bbb31ee7"
                }
            ],
            "cleanup": [
                "/include",
                "*.la",
                "*.a",
                "/lib/cmake",
                "/lib/pkgconfig"
            ]
        },
        {
            "name": "ffms2",
            "config-opts": [
                "--with-pic",
                "--disable-static",
                "--enable-shared"
            ],
            "cleanup": [
                "/include",
                "/lib/pkgconfig"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://github.com/FFMS/ffms2.git",
                    "branch": "master"
                }
            ]
        },
        {
            "name" : "libheif",
            "config-opts": [ "--disable-gdk-pixbuf" ],
            "cleanup": [ "/bin" ],
            "modules": [
                {
                    "name": "libde265",
                    "config-opts": [ "--disable-sherlock265" ],
                    "cleanup": [ "/bin" ],
                    "sources": [
                        {
                            "type": "archive",
                            "url": "https://github.com/strukturag/libde265/releases/download/v1.0.8/libde265-1.0.8.tar.gz",
                            "sha256": "24c791dd334fa521762320ff54f0febfd3c09fc978880a8c5fbc40a88f21d905"
                        }
                    ]
                }
            ],
            "sources" : [
                {
                    "url" : "https://github.com/strukturag/libheif/releases/download/v1.12.0/libheif-1.12.0.tar.gz",
                    "sha256" : "e1ac2abb354fdc8ccdca71363ebad7503ad731c84022cf460837f0839e171718",
                    "type" : "archive"
                }
            ]
        },
        {
            "name": "opencv",
            "buildsystem": "cmake-ninja",
            "builddir": true,
            "cleanup": [
                "/bin",
                "/include",
                "/lib/pkgconfig"
            ],
            "config-opts": [
                "-DCMAKE_BUILD_TYPE=Release",
                "-DBUILD_EXAMPLES=OFF",
                "-DOPENCV_GENERATE_PKGCONFIG=ON",
                "-DBUILD_DOCS=OFF",
                "-DBUILD_opencv_apps=OFF",
                "-DBUILD_opencv_python2=OFF",
                "-DBUILD_opencv_python3=OFF",
                "-DBUILD_PERF_TESTS=OFF",
                "-DBUILD_SHARED_LIBS=ON",
                "-DBUILD_TESTS=OFF",
                "-DENABLE_PRECOMPILED_HEADERS=OFF",
                "-DFORCE_VTK=OFF",
                "-DWITH_FFMPEG=OFF",
                "-DWITH_GDAL=OFF",
                "-DWITH_IPP=OFF",
                "-DWITH_OPENEXR=OFF",
                "-DWITH_OPENGL=OFF",
                "-DWITH_QT=OFF",
                "-DWITH_TBB=OFF",
                "-DWITH_XINE=OFF",
                "-DBUILD_JPEG=ON",
                "-DBUILD_TIFF=ON",
                "-DBUILD_PNG=ON"
            ],
            "sources": [
                {
                    "type": "archive",
                    "url": "https://github.com/opencv/opencv/archive/4.5.5.zip",
                    "sha256": "fb16b734db3a28e5119d513bd7c61ef417edf3756165dc6259519bb9d23d04e2"
                }
            ]
        },
        
        "shared-modules/intltool/intltool-0.51.json",
        {
            "name": "siril",
            "buildsystem": "meson",
            "config-opts": [
                "--buildtype=release"
            ],
            "post-install": [
                "sed -i s/text-x-seq/${FLATPAK_ID}.text-x-seq/ ${FLATPAK_DEST}/share/mime/packages/siril.xml",
                "mv ${FLATPAK_DEST}/share/mime/packages/siril.xml ${FLATPAK_DEST}/share/mime/packages/${FLATPAK_ID}.xml",
                "mv ${FLATPAK_DEST}/share/icons/hicolor/scalable/mimetypes/text-x-seq.svg ${FLATPAK_DEST}/share/icons/hicolor/scalable/mimetypes/${FLATPAK_ID}.text-x-seq.svg"
            ],
            "sources": [
                {
                    "type": "git",
                    "url": "https://gitlab.com/free-astro/siril.git",
                    "branch": "master"
                }
            ]
        }
    ]
}
